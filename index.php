<?php
//Borrar para produccion
error_reporting(E_ALL);
ini_set('display_errors', 1);

//SESION
session_start();

//URL
define("DIR_URL", "http://localhost/luna");

//directorio del proyecto
define("PROJECTPATH", dirname(__DIR__) . "\\luna");

//directorio app
define("APPPATH", PROJECTPATH . '/App');

//directorio de librerias externas
define("LIBSPATH", APPPATH . '/Libs');

//autoload con namespaces
function autoload_classes($class_name) {
    $filename = PROJECTPATH . '/' . str_replace('\\', '/', $class_name) . '.php';
    if(is_file($filename)) {
        include_once $filename;
    }
}
//registramos el autoload autoload_classes
spl_autoload_register('autoload_classes');

//instanciamos la app
$app = new \Core\App;

//lanzamos la app
$app->render();
?>