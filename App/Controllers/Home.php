<?php
namespace App\Controllers;
defined("APPPATH") OR die("Access denied");

use \Core\View,
	\App\Models\Principal as PrincipalModel;

class Home {

    public function index() {

        View::set("title", "Principal");
        View::set("message", "Hola Mundo!");
        View::render("principal");

    }

}
?>