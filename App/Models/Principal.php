<?php
namespace App\Models;
defined("APPPATH") OR die("Access denied");

use \Core\Database;
use \App\Interfaces\Crud;

class Buy implements Crud {

    public static function create($data) {

        try {

            $connection = Database::instance();
            $sql = "INSERT INTO `people`(`id`, `name`, `last_name`) VALUES (?, ?, ?)";
            $query = $connection->prepare($sql);
            $query->bindParam(1, $data['id'], \PDO::PARAM_INT);
            $query->bindParam(2, $data['name'], \PDO::PARAM_STR);
            $query->bindParam(3, $data['last_name'], \PDO::PARAM_STR);
            $query->execute();
            return $connection->lastInsertId();

        } catch(\PDOException $e) {

            print "Error!: " . $e->getMessage();

        }

    }

    public static function read($id) {

        try {

            $connection = Database::instance();
            $sql = "SELECT * FROM `people` WHERE `id` = ?";
            $query = $connection->prepare($sql);
            $query->bindParam(1, $id, \PDO::PARAM_INT);
            $query->execute();
            return $query->fetch();

        } catch(\PDOException $e) {

            print "Error!: " . $e->getMessage();

        }

    }

    public static function update($data) {

        try {

            $connection = Database::instance();
            $sql = "UPDATE `logs_login` SET `name` = ?, `last_name` = ? WHERE `id` = ?";
            $query = $connection->prepare($sql);
            $query->bindParam(1, $data['name'], \PDO::PARAM_STR);
            $query->bindParam(2, $data['last_name'], \PDO::PARAM_STR);
            $query->bindParam(3, $data['id'], \PDO::PARAM_INT);
            return $query->execute();

        } catch(\PDOException $e) {

            print "Error!: " . $e->getMessage();

        }

    }

    public static function delete($id) {

        try {

            $connection = Database::instance();
            $sql = "DELETE FROM `people` WHERE `id` = ?";
            $query = $connection->prepare($sql);
            $query->bindParam(1, $id, \PDO::PARAM_INT);
            return $query->execute();

        } catch(\PDOException $e) {

            print "Error!: " . $e->getMessage();

        }

    }

    public static function getAll() {

        try {

			$connection = Database::instance();
			$sql = "SELECT * FROM people";
			$query = $connection->prepare($sql);
			$query->execute();
			return $query->fetchAll();

		} catch(\PDOException $e) {

			print "Error!: " . $e->getMessage();

		}

    }


}
?>